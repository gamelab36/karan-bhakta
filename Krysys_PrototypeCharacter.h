// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Engine/DataTable.h"
#include "Components/StaticMeshComponent.h"
#include "ObjectMacros.h"
#include "Krysys_PrototypeCharacter.generated.h"


USTRUCT(BlueprintType)
struct FPlayerAttackMontage : public FTableRowBase
{
	GENERATED_BODY()


	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		 UAnimMontage* Montage;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int32 AnimSectionCount;
};


UENUM(BlueprintType) 
enum class EAttackType : uint8 {

	MELEE_HAMMER		UMETA(DisplayName = "Meele - Hammer")
	//MELEE_SWORD			UMETA(DisplayName = "Meele - Sword")

};




UCLASS(config=Game)
class AKrysys_PrototypeCharacter : public ACharacter
{
	GENERATED_BODY()

	/** Camera boom positioning the camera behind the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	/** Follow camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* FollowCamera;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Attack, meta = (AllowPrivateAccess = "true"))
		class UDataTable* AttackDataTable;
	float RunningTime;


public:
	AKrysys_PrototypeCharacter();


	virtual void BeginPlay() override;

	virtual void Tick(float DeltaTime) override;

	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseTurnRate;

	/** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseLookUpRate;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Attack)
		UAnimMontage* SmashAttackMontage;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Attack)
		UAnimMontage* SwordAttackMontage;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Attack)
		 UAnimMontage* DoubbleJumpMontage;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Weapons)
		class UStaticMeshComponent* WeaponIndex;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Weapons)
		class UStaticMeshComponent* BowIndex;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Weapons)
		bool IsSmashAttack;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Weapons)
		bool IsAiming;

protected:

	/** Resets HMD orientation in VR. */
	void OnResetVR();

	/** Called for forwards/backward input */
	void MoveForward(float Value);

	/** Called for side to side input */
	void MoveRight(float Value);

	/** 
	 * Called via input to turn at a given rate. 
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void TurnAtRate(float Rate);

	/**
	 * Called via input to turn look up/down at a given rate. 
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void LookUpAtRate(float Rate);

	/** Handler for when a touch input begins. */
	void TouchStarted(ETouchIndex::Type FingerIndex, FVector Location);

	/** Handler for when a touch input stops. */
	void TouchStopped(ETouchIndex::Type FingerIndex, FVector Location);

	virtual void OnLanded(FHitResult);
	
	
protected:
	// APawn interface
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	// End of APawn interface

private:
	void DoubbleJump();

	void SmashAttack();

	void SwordAttack();

	void ChangeWeapon();

	void DrawBow();

	void UnDrawBow();

	void Sprint();

	void StopSprint();


public:
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns FollowCamera subobject **/
	FORCEINLINE class UCameraComponent* GetFollowCamera() const { return FollowCamera; }
};

