// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "Krysys_PrototypeCharacter.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/Controller.h"
#include "GameFramework/SpringArmComponent.h"
#include "GameFramework/Character.h"
#include "Engine/Engine.h"
#include "ConstructorHelpers.h"
#include "Engine/EngineTypes.h"
#include <stdlib.h>

//////////////////////////////////////////////////////////////////////////
// AKrysys_PrototypeCharacter

AKrysys_PrototypeCharacter::AKrysys_PrototypeCharacter()
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	// Don't rotate when the controller rotates. Let that just affect the camera.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Character moves in the direction of input...	
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f); // ...at this rotation rate
	GetCharacterMovement()->JumpZVelocity = 600.f;
	GetCharacterMovement()->AirControl = 0.2f;

	// Create a camera boom (pulls in towards the player if there is a collision)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = 300.0f; // The camera follows at this distance behind the character	
	CameraBoom->bUsePawnControlRotation = true; // Rotate the arm based on the controller

	// Create a follow camera
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName); // Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	FollowCamera->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	WeaponIndex = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("WeaponIndex"));
	WeaponIndex->SetupAttachment(RootComponent);

	BowIndex = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("BowIndex"));
	BowIndex->SetupAttachment(RootComponent);


	static ConstructorHelpers::FObjectFinder<UAnimMontage> SmashAttackObject(TEXT("AnimMontage'/Game/Extra/LeapAttack_Root_Montage.LeapAttack_Root_Montage'"));

	static ConstructorHelpers::FObjectFinder<UAnimMontage> SwordAttackObject(TEXT("AnimMontage'/Game/Extra/SwordAttacksMontages.SwordAttacksMontages'"));

	static ConstructorHelpers::FObjectFinder<UStaticMesh> WeaponObject(TEXT("StaticMesh'/Game/Weapon_Pack/Mesh/Weapons/Weapons_Kit/SM_GreatHammer.SM_GreatHammer'"));

	static ConstructorHelpers::FObjectFinder<UAnimMontage> DoubbleJumpObject(TEXT("AnimMontage'/Game/Krysys/Montages/Double_Jump_Montage.Double_Jump_Montage'"));


	if (WeaponObject.Succeeded())
	{
		WeaponIndex->SetStaticMesh(WeaponObject.Object);
	}

	if (SmashAttackObject.Succeeded())
	{
		SmashAttackMontage = SmashAttackObject.Object;
	}

	if (SwordAttackObject.Succeeded())
	{
		SwordAttackMontage = SwordAttackObject.Object;
	}

	if (DoubbleJumpObject.Succeeded())
	{
		DoubbleJumpMontage = DoubbleJumpObject.Object;
	}

	

	
	// Note: The skeletal mesh and anim blueprint references on the Mesh component (inherited from Character) 
	// are set in the derived blueprint asset named MyCharacter (to avoid direct content references in C++)
}

void AKrysys_PrototypeCharacter::BeginPlay()
{
	Super::BeginPlay();

	const FAttachmentTransformRules AttachmentRules(EAttachmentRule::SnapToTarget, EAttachmentRule::SnapToTarget, EAttachmentRule::KeepWorld, false);

	WeaponIndex->AttachToComponent(GetMesh(), AttachmentRules, "Weapon");
	BowIndex->AttachToComponent(GetMesh(), AttachmentRules, "Bow");

}		


void AKrysys_PrototypeCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);


	if (ACharacter::JumpCurrentCount ==2)
	{
		DoubbleJump();
	}

}

// Input

void AKrysys_PrototypeCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// Set up gameplay key bindings
	check(PlayerInputComponent);
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &AKrysys_PrototypeCharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	PlayerInputComponent->BindAxis("MoveForward", this, &AKrysys_PrototypeCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AKrysys_PrototypeCharacter::MoveRight);

	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "turn" handles devices that provide an absolute delta, such as a mouse.
	// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("TurnRate", this, &AKrysys_PrototypeCharacter::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("LookUpRate", this, &AKrysys_PrototypeCharacter::LookUpAtRate);

	// handle touch devices
	PlayerInputComponent->BindTouch(IE_Pressed, this, &AKrysys_PrototypeCharacter::TouchStarted);
	PlayerInputComponent->BindTouch(IE_Released, this, &AKrysys_PrototypeCharacter::TouchStopped);

	// VR headset functionality
	PlayerInputComponent->BindAction("ResetVR", IE_Pressed, this, &AKrysys_PrototypeCharacter::OnResetVR);
	PlayerInputComponent->BindAction("AirAttack", IE_Pressed, this, &AKrysys_PrototypeCharacter::SmashAttack);

	//SwordAttack.
	PlayerInputComponent->BindAction("SlashAttack", IE_Pressed, this, &AKrysys_PrototypeCharacter::SwordAttack);
	PlayerInputComponent->BindAction("ChangeWeapon", IE_Pressed, this, &AKrysys_PrototypeCharacter::ChangeWeapon);

	// Draw/UnDraw Bow.
	PlayerInputComponent->BindAction("DrawBow", IE_Pressed, this, &AKrysys_PrototypeCharacter::DrawBow);
	PlayerInputComponent->BindAction("DrawBow", IE_Released, this, &AKrysys_PrototypeCharacter::UnDrawBow);

	// Sprint functionality.
	PlayerInputComponent->BindAction("Sprint", IE_Pressed, this, &AKrysys_PrototypeCharacter::Sprint);
	PlayerInputComponent->BindAction("Sprint", IE_Released, this, &AKrysys_PrototypeCharacter::StopSprint);

}


void AKrysys_PrototypeCharacter::OnResetVR()
{
	UHeadMountedDisplayFunctionLibrary::ResetOrientationAndPosition();
}

void AKrysys_PrototypeCharacter::TouchStarted(ETouchIndex::Type FingerIndex, FVector Location)
{
		Jump();
}

void AKrysys_PrototypeCharacter::TouchStopped(ETouchIndex::Type FingerIndex, FVector Location)
{
		StopJumping();
}

void AKrysys_PrototypeCharacter::OnLanded(FHitResult)
{
	ACharacter::GetCharacterMovement()->GravityScale = 1.0f; // if player touches ground gravity scale is set back to its normal value.
}


void AKrysys_PrototypeCharacter::DoubbleJump()
{
	
		PlayAnimMontage(DoubbleJumpMontage, 1.f); // Playing Doubble Jump Montage.
	
}

//
void AKrysys_PrototypeCharacter::ChangeWeapon()
{
	//TODO: Change weapons on Pressed Button.
}

void AKrysys_PrototypeCharacter::SmashAttack()
{

	


	if (ACharacter::JumpCurrentCount == 2 && IsAiming == false)	// This line checks if player current jump count is = 2 and is aiming false.
	{
		

		
		GEngine->AddOnScreenDebugMessage(-1, 4.5f, FColor::Red, "Smash Attack"); // Print String on screen.

		IsSmashAttack = true; // IsSmash attack true, by default its false.

		

		PlayAnimMontage(SmashAttackMontage, 1.f); // Playing Smash Attack montage.

	ACharacter::GetCharacterMovement()->GravityScale = 5.0f; // setting Player gravity scale. 
	
	}
}

void AKrysys_PrototypeCharacter::SwordAttack()
{
	if (ACharacter::JumpCurrentCount == 0 && IsAiming == false) // this checks that player is not in air  and aiming is false.
	{
		

		if (AttackDataTable)
		{
			static const FString  ContextString(TEXT("Attack Montage Context"));
			FPlayerAttackMontage* AttackMontage = AttackDataTable->FindRow<FPlayerAttackMontage>(FName(TEXT("Attack1")), ContextString, true);
			if (AttackMontage)
			{
				GEngine->AddOnScreenDebugMessage(-1, 4.5f, FColor::Red, __FUNCTION__);

				int MontageSectionIndex = rand() % AttackMontage->AnimSectionCount + 1;

				FString MontageSection = "Start_" + FString::FromInt(MontageSectionIndex);

				PlayAnimMontage(AttackMontage->Montage, 1.f, FName(*MontageSection));
			}
		}

	}
			
}

														// This function is to Equip Bow.

void AKrysys_PrototypeCharacter::DrawBow() 
{
	IsAiming = true;
	bUseControllerRotationYaw = true; 

}
														// This function is to UnEquip Bow.
void AKrysys_PrototypeCharacter::UnDrawBow() 
{
	IsAiming = false;
	bUseControllerRotationYaw = false;

}
														// This function is to sprint.
void AKrysys_PrototypeCharacter::Sprint()
{
	GetCharacterMovement()->MaxWalkSpeed = 600;
}
														// This function is to stop sprint.
void AKrysys_PrototypeCharacter::StopSprint()
{
	GetCharacterMovement()->MaxWalkSpeed = 300;
}

void AKrysys_PrototypeCharacter::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void AKrysys_PrototypeCharacter::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

void AKrysys_PrototypeCharacter::MoveForward(float Value)
{
	if ((Controller != NULL) && (Value != 0.0f))
	{
		// find out which way is forward
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get forward vector
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		AddMovementInput(Direction, Value);
	}
}

void AKrysys_PrototypeCharacter::MoveRight(float Value)
{
	if ( (Controller != NULL) && (Value != 0.0f) )
	{
		// find out which way is right
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);
	
		// get right vector 
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		// add movement in that direction
		AddMovementInput(Direction, Value);
	}
}
